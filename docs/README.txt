README.txt
(by Peter Ross)


This file describes some fake data that illustrates the kind of
allocation problem that commonly occurs in EC research calls.

The process of creating this data took some inspiration from
an old, real-world example: in particular the number and grouping
of keywords owes a lot to a particular call and the general patterns
of occurrences and co-occurrences should not be unrealistic, even
though random number generation has been used extensively!


THE DATA:

There are three files, each in CSV form:
  anonymized-expert-data.csv
  anonymized-proposal-data.csv
  stats.csv
  
The data in "anonymized-expert-data.csv" contains data about a hypothetical
initial pool of experts for a panel. Here are some details:

-- there are 400 experts, of whom slightly more than half are female.

-- nationalities are each one of nat01, nat02, .., nat36, but very
   unevenly distributed. If you check, you will find that nat12, nat15,
   nat17, nat23, nat243, nat25, nat27, nat31, nat32, nat34 have
   double-digit numbers each (up to 55, in fact) and all the rest have
   at most single-digit numbers of occurrences. Ten nationalities
   each have a single representative.

-- each expert has declared his/her expertise as a set of keywords,
   where each keyword has the form kw_Lnnn where the L is a letter in
   the set A..G and nnn is a three-digit number -- for example,
   kw_A301 or kw_G202; the A set is the largest. Each of the eight
   letters indicates one of an extremely broad classification into
   highest-level areas. This means that if you cannot find an
   available expert with keyword (say) kw_B104 then, as very last
   resort, you could use some available expert who has a keyword
   kw_B1nn (that is, has the same letter *and* first digit), and so 
   on. But this must only be used as a very last option, if and only 
   if all else fails.
     Each expert has also listed a level of expertise associated with
   each keyword that he/she chose, as an integer that follows the
   keyword.  The integers used are 1 = excellent, 2 = very good, 3
   = good.  In each row, all the 1s appear first, then the 2s, then
   the 3s.
   
-- some experts have previous experience of reviewing in this call
   in previous years ("experienced"); a few have reviewed in other
   calls ("new_to_call"); some have never reviewed before
   ("new_expert").

-- the family and first names have been generated at random using real
   family and first names from the USA 2010 census, which recorded
   88799 unique family names, 1213 male first names and 4274 female
   first names. First names are gender-appropriate according to the US
   census data, even though they may not always seem to be so. For
   ease of reference, each expert also has a unique identifier, one
   of e001..e400. First and family names are therefore not really
   needed at all, but they are included to provide a modest degree
   of personalisation of the data. In real problems, the names are far
   easier to read and remember than the unique IDs are. 

-- the fields in each row are: expert ID, family name, first name,
   gender, nationality, experieced reviewer?, has industrial
   experience?, has PhD or equivalent?, and then a number of
   keyword/expertise-level pairs. All the excellent (=1) keywords
   appears before any very-good (=2) ones, and those appear before
   any good (=3) ones.
   
The data in "anonymized-proposal-data.csv" contains keyword information about
500 proposals, identified as prop001 .. prop500. The same set of
keywords as above is used. Each proposal has specified up to five
keywords, but maybe fewer. The order in which the keywords are
specified for each proposal matters; as in real calls, proposers are
asked to state the most important keyword first, second most important
next, and so on. The fields in any row are: proposal ID, then the
keywords, in decreasing order of importance.

In both files of fake data, no keyword appears more than once in any
row.

The data in "stats.csv" consists of counts of the number of times each
keyword/rating pair appears among experts and also the number of times
that the keyword is listed among the proposals. For example, the
third line is:
  kw_A102,11,15,19,45,27
which tells you that kw_A102 was mentioned by 45 experts and 27
proposals and that of those 45 experts, 11 rated themselves
excellent, 15 rated themselves very good and 19 rated themselves good
(and of course 11+15+19=45). The data in this file tells you a variety
of interesting things:

-- among the 375 keywords, there are 142 keywords that were selected
   by one or more experts but not selected by any proposal, so those
   keywords can be ignored. Therefore only 375-142=233 of them are of
   interest;

-- there are twenty proposal keywords (each associated with only one
   proposal) that no expert has chosen. Either these keywords will
   have to be left uncovered, or the 'last resort' option (see above)
   will have to be used. But overall, only 43 keywords were such that
   the keyword was selected by fewer experts than the number of
   occurrences of the keyword among the proposals. For example,
   kw_D106 appears ten times among proposals but only five times among
   experts, so maybe each of those experts needs to be assigned to two
   of those proposals. Also, there are 15 keywords that each appear
   exactly once among proposals and once among experts, so those
   proposals should be assigned to those experts;
 
-- some keywords are very popular among experts but very rare among
   proposals. For example, kw_B104 occurs 59 times among the experts
   but only twice among the proposals.

Note that among the 500 proposals thare are 1932 instances of keywords
that should, ideally, be covered. But among the experts there are only
1310 instances of keywords rated as excellent (=100), and 1361
instances rated as very good (=67).


THE GENERAL PROBLEM:

The typical task is to assign three 'appropriate' experts to each
proposal, such that:

(a) each proposal keyword is covered, as far as possible, and ideally
    at least one of the three assigned experts will have rated his/her
    expertise in the keyword as excellent (=1) or very good (=2),
    but if this is not practicable then it is OK to cover a keyword
    with an expert whose expertise in that keyword is good (=3).
    Ideally, the first keyword(s) of each proposal each should be covered
    by at least one of the experts with excellent (=100) expertise in the
    keyword(s). Obviously, one expert can cover more than one keyword,
    and one keyword may be covered by more than one of the three
    chosen experts.

(b) ideally, the three experts assigned to a proposal should not all
    be of the same gender, and should not all be of the same
    nationality.

(c) ideally, the three experts should include at least one person
    with industrial experience and at least one person who has been
    educated to PhD level.

(d) no expert should be assigned to fewer than 6 proposals or more
    than 12. Since there are 500 proposals, this also means that there
    will be between (500 x 3)/12 = 125 and (500 x 3) / 6 = 250 experts
    selected in all, and therefore some experts (between 150 and 275
    of the initial pool of 400) will not be used at all.
      In general, the minimum and maximum numbers (6 and 12 here) would
    be call-specific.
   
For administrative and quality-control reasons it is preferable to try
to keep the number of experts relatively low, that is, much closer to
125 than to 250.

Because this is a 'soft' optimisation problem. no indicative solution
is provided.
